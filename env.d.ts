interface ImportMetaEnv extends Readonly<Record<string, string>> {
    readonly VITE_GOOGLE_RECAPTCHA_SITE_KEY: string
    // more env variables...
}

interface ImportMeta {
    readonly env: ImportMetaEnv
}
