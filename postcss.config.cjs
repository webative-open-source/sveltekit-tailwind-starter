const tailwindcssNesting = require('tailwindcss/nesting')
const tailwindcss = require('tailwindcss')
const autoprefixer = require('autoprefixer')
const postcssImport = require('postcss-import')
const postcssUrl = require('postcss-url')
const postcssScrollbar = require('postcss-scrollbar')
const cssnano = require('cssnano')

const mode = process.env.NODE_ENV
const dev = mode === 'development'

const config = {
  plugins: [
    // Some plugins, like tailwindcss/nesting, need to run before Tailwind
    postcssImport({
      /* ...options */
    }),

    postcssUrl({
      /* ...options */
    }),

    postcssScrollbar(),

    tailwindcssNesting(),

    tailwindcss(),

    // But others, like autoprefixer, need to run after
    autoprefixer(),

    !dev &&
      cssnano({
        preset: ['default', { discardComments: { removeAll: true } }],
      }),
  ],
}

module.exports = config
