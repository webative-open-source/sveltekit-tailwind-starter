const defaultTheme = require('tailwindcss/defaultTheme')

const config = {
  content: ['./src/**/*.{html,svelte,js}'],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Catamaran', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        brand: {
          950: '#121214',
          900: '#17181a',
          800: '#27292C',
          700: '#37393E',
          600: '#46494F',
          500: '#565961',
          400: '#DCDDE0',
          300: '#F4F4F5',
          200: '#F8FAFD',
        },
      },
    },
  },
  plugins: [require('@tailwindcss/forms')],
}

module.exports = config
