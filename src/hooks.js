import { minify } from 'html-minifier'
import { prerendering } from '$app/env'

const minificationOpions = {
  collapseBooleanAttributes: true,
  collapseInlineTagWhitespace: true,
  collapseWhitespace: true,
  conservativeCollapse: true,
  decodeEntities: true,
  html5: true,
  ignoreCustomComments: [/^#/],
  minifyCSS: true,
  minifyJS: false,
  removeAttributeQuotes: true,
  removeComments: true,
  removeOptionalTags: true,
  removeRedundantAttributes: true,
  removeScriptTypeAttributes: true,
  removeStyleLinkTypeAttributes: true,
  sortAttributes: true,
  sortClassName: true,
  removeEmptyElements: true,
}

const { NODE_ENV } = process.env
const dev = NODE_ENV === 'development'

export async function handle({ event, resolve }) {
  const response = await resolve(event)

  if (dev) return response

  // Minify the response body
  if (prerendering && response.headers.get('content-type') === 'text/html') {
    const body = await response.text()

    return new Response(minify(body, minificationOpions), {
      status: response.status,
      headers: response.headers,
    })
  }

  return response
}
