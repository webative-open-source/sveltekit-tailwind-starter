const variables = {
  recaptchaSiteKey: import.meta.env.VITE_GOOGLE_RECAPTCHA_SITE_KEY,
  googleMapsSiteKey: import.meta.env.VITE_GOOGLE_MAPS_SITE_KEY,
}

export { variables as default }
