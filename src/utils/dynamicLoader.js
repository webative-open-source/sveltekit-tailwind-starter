/**
 * Automatically detects the type of the resource by looking at the URL file extensions
 *
 * @param {string} url The resource URL
 * @returns {('script'|'stylesheet')} The resource type
 */
function getResourceType(url) {
  if (!url || typeof url !== 'string') return 'script'
  const isStylesheet = /(\.(css)(?=\?|$))/.test(url)
  return isStylesheet ? 'stylesheet' : 'script'
}

/**
 * Asserts proper URLs
 *
 * @param {Object[]} urls - A list of URLs with their attributes
 * @returns {Object[]} Proper formatted array of URLs
 */
function assertUrls(urls) {
  // assert proper urls
  if (!Array.isArray(urls)) {
    if (typeof urls !== 'string' || urls === '') return []
    urls = [
      {
        type: getResourceType(urls),
        src: urls,
      },
    ]
  }

  return urls
    .map(item => {
      // return a throwaway object if item is falsy
      if (!item)
        return {
          type: undefined,
          src: '',
        }

      if (typeof item === 'object') {
        if (!item || !item.src || typeof item.src !== 'string') item.src = ''
        if (
          !item ||
          !item.type ||
          typeof item.type !== 'string' ||
          (item.type !== 'script' && item.type !== 'stylesheet')
        ) {
          item.type = getResourceType(item.src)
        }

        return item
      }

      const src = typeof item === 'string' ? item : ''
      return {
        type: getResourceType(src),
        src,
      }
    })
    .filter(
      item =>
        item.src !== '' &&
        (item.type === 'script' || item.type === 'stylesheet') &&
        // don't add the element if it is already present in the DOM
        !document.querySelector(`${item.type}[src="${item.src}"]`),
    )
}

/**
 * Allows asynchronous loading of scripts and styles:
 *
 * * Using a test to ensure that the code is only loaded once
 * * Running a callback once the script is loaded
 * * Running a callback if the script is already loaded
 * * Not blocking the main thread
 *
 * @param {Object} params
 * @param {Object[]} params.urls A list of URLs with their attributes
 * @param {string} params.urls[].type The type of the resource (i.e. script or stylesheet)
 * @param {string} params.urls[].src URL of the external resource to be loaded
 * @param {Object[]} params.urls[].attributes Attributes to add to the tag (e.g. `async`, `defer`, etc.). `async` and `defer` are the defaults
 * @param {string} params.urls[].attributes[].name An attribute name
 * @param {string} params.urls[].attributes[].value An attribute value
 * @param {function(): boolean} [params.testCondition] A method of checking if the library is ready
 * @param {boolean} [params.allowIframe] Allow loading the script inside an iframe
 * @param {function(): void} [params.callback] A function to call when the library is ready
 */

export function loader({ urls, testCondition, allowIframe, callback }) {
  // Don't load any scripts if we are in an iframe, which is the default
  if (window && window.self !== window.top && !allowIframe) return

  // assert proper options
  if (typeof callback !== 'function') callback = false
  if (typeof testCondition !== 'function') testCondition = false

  // Run the callback and return if script has already been loaded
  if (!!testCondition && testCondition()) {
    if (callback) callback()
    return
  }

  // assert proper urls
  urls = assertUrls(urls)
  if (urls.length === 0) return

  let tally = urls.length
  const maybeCallback = () => {
    if (!callback) return
    tally = --tally
    if (tally < 1) callback()
  }

  urls.forEach(({ type, src, attributes = [] }) => {
    const isScript = type === 'script'
    const tag = document.createElement(isScript ? 'script' : 'link')

    attributes = [
      { name: 'async', value: true },
      { name: 'defer', value: true },
      ...attributes,
    ]

    if (isScript) {
      tag.src = src

      if (attributes.length) {
        attributes.forEach(attribute =>
          tag.setAttribute(attribute.name, attribute.value),
        )
      }
    } else {
      tag.rel = 'stylesheet'
      tag.href = src
    }

    tag.onload = maybeCallback
    document.body.appendChild(tag)
  })
}
