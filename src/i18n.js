// @typescript-eslint/ban-ts-comment
import { init, register, waitLocale } from 'svelte-intl-precompile'

const INIT_OPTIONS = {
  fallbackLocale: 'ro',
  initialLocale: 'ro',
  loadingDelay: 0,
}

register('en', () => import('./resources/lang/en.json'))
register('ro', () => import('./resources/lang/ro.json'))
init({ ...INIT_OPTIONS })

export const bootLocale = async () => waitLocale()
